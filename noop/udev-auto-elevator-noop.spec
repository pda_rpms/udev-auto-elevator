Name:		udev-auto-elevator-noop
Summary:	Sets noop scheduler for non rotational disks
Summary(ru_RU.UTF-8): Устанавливает планировщик noop для не вращающихся дисков
Version:	1.0.1
Release:	5%{?dist}
License:	Public Domain
URL:		https://gitlab.com/pda_rpms/udev-auto-elevator
Group:		System Environment/Base
Packager:	Dmitriy Pomerantsev <pda2@yandex.ru>
Source0:	65-elevator-noop.rules
BuildArch:	noarch
BuildRoot:	%{_tmppath}/%{name}-%{version}-root
%if 0%{?rhel} == 7
Requires:	systemd
%else
Requires:	systemd-udev
%endif
%if 0%{?rhel} > 8 || 0%{?fedora} >= 30
BuildRequires:	systemd-rpm-macros
%endif
Conflicts:	udev-auto-elevator-deadline

AutoReqProv:	no

%description
The udev rule that automatically sets the noop scheduler for all non
rotational block devices.

%description -l ru_RU.UTF-8
Правило udev, автоматически устанавливающее планировщик noop для всех
не вращающихся блочных устройств.

%build
# None required

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%{__mkdir_p} %{buildroot}%{_udevrulesdir}
%{__install} -D -m0644 %{SOURCE0} %{buildroot}%{_udevrulesdir}/

%post
udevadm control --reload-rules
udevadm trigger --subsystem-match='block'

%postun
udevadm control --reload-rules
udevadm trigger --subsystem-match='block'

%files
%defattr(-,root,root)
%{_udevrulesdir}/*

%changelog
* Mon Dec 02 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.1 - 5
- Added build dependency.

* Mon Dec 02 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.1 - 4
- Udev rules moved to system location.

* Thu Nov 28 2019 Dmitriy Pomerantsev <pda2@yandex.ru> - 1.0.0 - 3
- License changed for Public Domain.

* Sun Nov 25 2018 Dmitriy Pomerantsev <pda2@yandex.ru>
- Added rule for devices which have an own queue.
- Fixed CentOS 7 dependencies.

* Sat Nov 24 2018 Dmitriy Pomerantsev <pda2@yandex.ru>
- Removed obsolete %clean section.

* Thu Nov 22 2018 Dmitriy Pomerantsev <pda2@yandex.ru>
- Initial build.
